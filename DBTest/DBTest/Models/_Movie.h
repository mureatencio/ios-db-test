// DO NOT EDIT. This file is machine-generated and constantly overwritten.
// Make changes to Movie.h instead.

#import <CoreData/CoreData.h>

extern const struct MovieAttributes {
	__unsafe_unretained NSString *creationDate;
	__unsafe_unretained NSString *name;
} MovieAttributes;

extern const struct MovieRelationships {
	__unsafe_unretained NSString *movieCategory;
	__unsafe_unretained NSString *ratings;
} MovieRelationships;

@class MovieCategory;
@class Rating;

@interface MovieID : NSManagedObjectID {}
@end

@interface _Movie : NSManagedObject {}
+ (id)insertInManagedObjectContext:(NSManagedObjectContext*)moc_;
+ (NSString*)entityName;
+ (NSEntityDescription*)entityInManagedObjectContext:(NSManagedObjectContext*)moc_;
@property (nonatomic, readonly, strong) MovieID* objectID;

@property (nonatomic, strong) NSDate* creationDate;

//- (BOOL)validateCreationDate:(id*)value_ error:(NSError**)error_;

@property (nonatomic, strong) NSString* name;

//- (BOOL)validateName:(id*)value_ error:(NSError**)error_;

@property (nonatomic, strong) MovieCategory *movieCategory;

//- (BOOL)validateMovieCategory:(id*)value_ error:(NSError**)error_;

@property (nonatomic, strong) NSSet *ratings;

- (NSMutableSet*)ratingsSet;

@end

@interface _Movie (RatingsCoreDataGeneratedAccessors)
- (void)addRatings:(NSSet*)value_;
- (void)removeRatings:(NSSet*)value_;
- (void)addRatingsObject:(Rating*)value_;
- (void)removeRatingsObject:(Rating*)value_;

@end

@interface _Movie (CoreDataGeneratedPrimitiveAccessors)

- (NSDate*)primitiveCreationDate;
- (void)setPrimitiveCreationDate:(NSDate*)value;

- (NSString*)primitiveName;
- (void)setPrimitiveName:(NSString*)value;

- (MovieCategory*)primitiveMovieCategory;
- (void)setPrimitiveMovieCategory:(MovieCategory*)value;

- (NSMutableSet*)primitiveRatings;
- (void)setPrimitiveRatings:(NSMutableSet*)value;

@end
