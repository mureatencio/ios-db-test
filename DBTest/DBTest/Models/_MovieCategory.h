// DO NOT EDIT. This file is machine-generated and constantly overwritten.
// Make changes to MovieCategory.h instead.

#import <CoreData/CoreData.h>

extern const struct MovieCategoryAttributes {
	__unsafe_unretained NSString *creationDate;
	__unsafe_unretained NSString *name;
} MovieCategoryAttributes;

extern const struct MovieCategoryRelationships {
	__unsafe_unretained NSString *movies;
} MovieCategoryRelationships;

@class Movie;

@interface MovieCategoryID : NSManagedObjectID {}
@end

@interface _MovieCategory : NSManagedObject {}
+ (id)insertInManagedObjectContext:(NSManagedObjectContext*)moc_;
+ (NSString*)entityName;
+ (NSEntityDescription*)entityInManagedObjectContext:(NSManagedObjectContext*)moc_;
@property (nonatomic, readonly, strong) MovieCategoryID* objectID;

@property (nonatomic, strong) NSDate* creationDate;

//- (BOOL)validateCreationDate:(id*)value_ error:(NSError**)error_;

@property (nonatomic, strong) NSString* name;

//- (BOOL)validateName:(id*)value_ error:(NSError**)error_;

@property (nonatomic, strong) NSSet *movies;

- (NSMutableSet*)moviesSet;

@end

@interface _MovieCategory (MoviesCoreDataGeneratedAccessors)
- (void)addMovies:(NSSet*)value_;
- (void)removeMovies:(NSSet*)value_;
- (void)addMoviesObject:(Movie*)value_;
- (void)removeMoviesObject:(Movie*)value_;

@end

@interface _MovieCategory (CoreDataGeneratedPrimitiveAccessors)

- (NSDate*)primitiveCreationDate;
- (void)setPrimitiveCreationDate:(NSDate*)value;

- (NSString*)primitiveName;
- (void)setPrimitiveName:(NSString*)value;

- (NSMutableSet*)primitiveMovies;
- (void)setPrimitiveMovies:(NSMutableSet*)value;

@end
