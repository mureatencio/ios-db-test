// DO NOT EDIT. This file is machine-generated and constantly overwritten.
// Make changes to MovieCategory.m instead.

#import "_MovieCategory.h"

const struct MovieCategoryAttributes MovieCategoryAttributes = {
	.creationDate = @"creationDate",
	.name = @"name",
};

const struct MovieCategoryRelationships MovieCategoryRelationships = {
	.movies = @"movies",
};

@implementation MovieCategoryID
@end

@implementation _MovieCategory

+ (id)insertInManagedObjectContext:(NSManagedObjectContext*)moc_ {
	NSParameterAssert(moc_);
	return [NSEntityDescription insertNewObjectForEntityForName:@"MovieCategory" inManagedObjectContext:moc_];
}

+ (NSString*)entityName {
	return @"MovieCategory";
}

+ (NSEntityDescription*)entityInManagedObjectContext:(NSManagedObjectContext*)moc_ {
	NSParameterAssert(moc_);
	return [NSEntityDescription entityForName:@"MovieCategory" inManagedObjectContext:moc_];
}

- (MovieCategoryID*)objectID {
	return (MovieCategoryID*)[super objectID];
}

+ (NSSet*)keyPathsForValuesAffectingValueForKey:(NSString*)key {
	NSSet *keyPaths = [super keyPathsForValuesAffectingValueForKey:key];

	return keyPaths;
}

@dynamic creationDate;

@dynamic name;

@dynamic movies;

- (NSMutableSet*)moviesSet {
	[self willAccessValueForKey:@"movies"];

	NSMutableSet *result = (NSMutableSet*)[self mutableSetValueForKey:@"movies"];

	[self didAccessValueForKey:@"movies"];
	return result;
}

@end

