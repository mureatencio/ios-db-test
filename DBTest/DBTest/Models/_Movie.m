// DO NOT EDIT. This file is machine-generated and constantly overwritten.
// Make changes to Movie.m instead.

#import "_Movie.h"

const struct MovieAttributes MovieAttributes = {
	.creationDate = @"creationDate",
	.name = @"name",
};

const struct MovieRelationships MovieRelationships = {
	.movieCategory = @"movieCategory",
	.ratings = @"ratings",
};

@implementation MovieID
@end

@implementation _Movie

+ (id)insertInManagedObjectContext:(NSManagedObjectContext*)moc_ {
	NSParameterAssert(moc_);
	return [NSEntityDescription insertNewObjectForEntityForName:@"Movie" inManagedObjectContext:moc_];
}

+ (NSString*)entityName {
	return @"Movie";
}

+ (NSEntityDescription*)entityInManagedObjectContext:(NSManagedObjectContext*)moc_ {
	NSParameterAssert(moc_);
	return [NSEntityDescription entityForName:@"Movie" inManagedObjectContext:moc_];
}

- (MovieID*)objectID {
	return (MovieID*)[super objectID];
}

+ (NSSet*)keyPathsForValuesAffectingValueForKey:(NSString*)key {
	NSSet *keyPaths = [super keyPathsForValuesAffectingValueForKey:key];

	return keyPaths;
}

@dynamic creationDate;

@dynamic name;

@dynamic movieCategory;

@dynamic ratings;

- (NSMutableSet*)ratingsSet {
	[self willAccessValueForKey:@"ratings"];

	NSMutableSet *result = (NSMutableSet*)[self mutableSetValueForKey:@"ratings"];

	[self didAccessValueForKey:@"ratings"];
	return result;
}

@end

