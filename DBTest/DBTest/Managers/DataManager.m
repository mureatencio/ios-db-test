
#import "DataManager.h"
#import "MovieCategory.h"
#import "Movie.h"
#import "Rating.h"

@implementation DataManager

//Singleton Instance
+ (DataManager *)sharedInstance {
    static DataManager *instance;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        instance = [[self alloc] init];
    });
    return instance;
}

//Inserting movie categories
- (void)preloadDatabase {
    if([self movieCategories].count==0){
        NSArray *categoriesArray = [NSArray arrayWithObjects:@"Accion",@"Historia",@"Animadas",@"Suspenso",@"Terror",nil];
        for (NSString *categoryName in categoriesArray) {
            MovieCategory *movieCategory = [MovieCategory MR_createEntity];
            movieCategory.name = categoryName;
            movieCategory.creationDate = [NSDate date];
            [[NSManagedObjectContext defaultContext] MR_saveToPersistentStoreWithCompletion:nil];
        }
    }
}

//create rating with a number
- (void)createRatingWithRating:(NSNumber *)rating forMovie:(Movie *)movie {
    Rating *ratingEntity = [Rating MR_createEntity];
    ratingEntity.quantity = rating;
    ratingEntity.creationDate = [NSDate date];
    ratingEntity.movie = movie;
    [[NSManagedObjectContext defaultContext] MR_saveToPersistentStoreWithCompletion:nil];
}

//get all movie categories
- (NSArray *)movieCategories {
    return [MovieCategory findAll];
}

//create movie with data
- (void)createMovieWithName:(NSString *)name category:(MovieCategory *)category{
    Movie *movie = [Movie MR_createEntity];
    movie.name = name;
    movie.creationDate = [NSDate date];
    movie.movieCategory = category;
    [[NSManagedObjectContext defaultContext] MR_saveToPersistentStoreWithCompletion:nil];
}

//delete a movie
- (void)removeMovie:(Movie *)movie {
    [movie MR_deleteEntity];
    [[NSManagedObjectContext defaultContext] MR_saveToPersistentStoreWithCompletion:nil];
}

//get all the ratings for a movie
- (NSArray *)getAllRatingsForMovie:(Movie *)movie {
    return [Rating findAllWithPredicate:[NSPredicate predicateWithFormat:@"movie == %@", movie]];
}



@end
