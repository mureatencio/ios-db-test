
#import <Foundation/Foundation.h>

@class Movie;
@class MovieCategory;

@interface DataManager : NSObject

+ (DataManager *)sharedInstance;

- (void)preloadDatabase;
- (void)createRatingWithRating:(NSNumber *)rating forMovie:(Movie *)movie;
- (NSArray *)movieCategories;
- (void)createMovieWithName:(NSString *)name category:(MovieCategory *)category;
- (void)removeMovie:(Movie *)movie;
- (NSArray *)getAllRatingsForMovie:(Movie *)movie;

@end
