
#import "AppDelegate.h"
#import "DataManager.h"
#import "HomeViewController.h"

@interface AppDelegate ()

@end

@implementation AppDelegate

- (BOOL)application:(UIApplication *)application didFinishLaunchingWithOptions:(NSDictionary *)launchOptions {
    //setup magical record
    [MagicalRecord setupCoreDataStackWithStoreNamed:@"test"];
    
    //set navigation
    self.window = [[UIWindow alloc] initWithFrame:[[UIScreen mainScreen] bounds]];
    self.window.rootViewController.modalPresentationStyle = UIModalPresentationCurrentContext;
    UINavigationController *navController = [UINavigationController new];
    HomeViewController *homeVC = [HomeViewController new];
    navController.viewControllers = @[homeVC];
    self.window.rootViewController = navController;
    [[UINavigationBar appearance]setShadowImage:[[UIImage alloc] init]];
    
    //enable magical record shorthand
    [MagicalRecord enableShorthandMethods];
    
    //prepopulate the database
    [[DataManager sharedInstance] preloadDatabase];

    [self.window makeKeyAndVisible];
    return YES;
}

@end
