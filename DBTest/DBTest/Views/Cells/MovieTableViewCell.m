
#import "MovieTableViewCell.h"

@implementation MovieTableViewCell

- (void)awakeFromNib {
}

//return cell identifier
+ (NSString *)reuseIdentifier{
    return @"MovieTableViewCellIdentifier";
}

//return desired height
+ (CGFloat)cellHeight{
    return 94.0f;
}

@end
