
#import <UIKit/UIKit.h>

@interface AddMovieViewController : UIViewController

- (IBAction)didPressSave:(id)sender;
- (IBAction)didPressCancel:(id)sender;

@end

