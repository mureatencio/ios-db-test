
#import "RateMovieViewController.h"
#import "HCSStarRatingView.h"
#import "MovieCategory.h"
#import "DataManager.h"
#import "Rating.h"

@interface RateMovieViewController ()
@property (weak, nonatomic) IBOutlet UILabel *nameLabel;
@property (weak, nonatomic) IBOutlet UILabel *categoryLabel;
@property (weak, nonatomic) IBOutlet UILabel *totalRatingsLabel;
@property (weak, nonatomic) IBOutlet UILabel *averageLabel;
@property (weak, nonatomic) IBOutlet UIView *ratingContainer;
@property (strong, nonatomic) HCSStarRatingView *starRatingView;

@end

@implementation RateMovieViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    self.title = @"Rate movie";
    
    UIBarButtonItem *rateButton = [[UIBarButtonItem alloc] initWithTitle:@"Rate" style:UIBarButtonItemStylePlain target:self action:@selector(didPressRateButton)];
    self.navigationItem.rightBarButtonItem = rateButton;
    
    [self setupInfo];
    
}

-(void)setupInfo{
    self.nameLabel.text = self.movie.name;
    self.categoryLabel.text = self.movie.movieCategory.name;
    self.totalRatingsLabel.text = [NSString stringWithFormat:@"%lu",(unsigned long)self.ratings.count];
    CGFloat total = 0;
    for (Rating *rating in self.ratings) {
        total += [rating.quantity floatValue];
    }
    self.averageLabel.text = [NSString stringWithFormat:@"%.2f",total/self.ratings.count];
    
}

-(void)viewWillLayoutSubviews{
    [super viewWillLayoutSubviews];
    self.starRatingView = [[HCSStarRatingView alloc] init];
    self.starRatingView.maximumValue = 5;
    self.starRatingView.minimumValue = 1;
    self.starRatingView.value = 1;
    self.starRatingView.tintColor = [UIColor redColor];
    self.starRatingView.translatesAutoresizingMaskIntoConstraints = NO;
    [self.ratingContainer addSubview:self.starRatingView];
    
    // Width constraint
    [self.ratingContainer addConstraint:[NSLayoutConstraint constraintWithItem:self.starRatingView
                                                          attribute:NSLayoutAttributeWidth
                                                          relatedBy:NSLayoutRelationEqual
                                                             toItem:self.ratingContainer
                                                          attribute:NSLayoutAttributeWidth
                                                         multiplier:1
                                                           constant:0]];
    
    // Height constraint
    [self.ratingContainer addConstraint:[NSLayoutConstraint constraintWithItem:self.starRatingView
                                                          attribute:NSLayoutAttributeHeight
                                                          relatedBy:NSLayoutRelationEqual
                                                             toItem:self.ratingContainer
                                                          attribute:NSLayoutAttributeHeight
                                                         multiplier:1
                                                           constant:0]];
    
    // Center horizontally
    [self.ratingContainer addConstraint:[NSLayoutConstraint constraintWithItem:self.starRatingView
                                                          attribute:NSLayoutAttributeCenterX
                                                          relatedBy:NSLayoutRelationEqual
                                                             toItem:self.ratingContainer
                                                          attribute:NSLayoutAttributeCenterX
                                                         multiplier:1.0
                                                           constant:0.0]];
    
    // Center vertically
    [self.ratingContainer addConstraint:[NSLayoutConstraint constraintWithItem:self.starRatingView
                                                          attribute:NSLayoutAttributeCenterY
                                                          relatedBy:NSLayoutRelationEqual
                                                             toItem:self.ratingContainer
                                                          attribute:NSLayoutAttributeCenterY
                                                         multiplier:1.0
                                                           constant:0.0]];

}

- (void)didPressRateButton{
    [[DataManager sharedInstance] createRatingWithRating:@(self.starRatingView.value) forMovie:self.movie];
    [self.navigationController popViewControllerAnimated:YES];
}

@end
