
#import "AddMovieViewController.h"
#import "DataManager.h"
#import "MovieCategory.h"

@interface AddMovieViewController ()<UIGestureRecognizerDelegate, UIPickerViewDataSource, UIPickerViewDelegate>

@property (weak, nonatomic) IBOutlet UIPickerView *categoriesPickerView;
@property (weak, nonatomic) IBOutlet UITextField *nameTextView;
@property (weak, nonatomic) IBOutlet UIButton *saveButton;
@property (nonatomic, strong) NSArray *categories;

@end

@implementation AddMovieViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    self.categories = [[DataManager sharedInstance] movieCategories];
    
    self.view.userInteractionEnabled = YES;
    [self toggleSaveButton:NO];
    self.categoriesPickerView.dataSource = self;
    self.categoriesPickerView.delegate = self;
    [self.nameTextView addTarget:self
                             action:@selector(textFieldDidChange)
                   forControlEvents:UIControlEventEditingChanged];
}

- (void)textFieldDidChange{
    //Disable search button if textfield has no text
    [self toggleSaveButton:![self.nameTextView.text isEqualToString:@""]];
}

-(void)touchesBegan:(NSSet *)touches withEvent:(UIEvent *)event{
    //Hides keyboard if user taps outside of it
    [self.nameTextView resignFirstResponder];
    for (UIView *view in self.view.subviews)
        [view resignFirstResponder];
}

- (IBAction)didPressSave:(id)sender{
    NSInteger categorySelectedIndex = [self.categoriesPickerView selectedRowInComponent:0];
    MovieCategory *category = self.categories[categorySelectedIndex];
    [[DataManager sharedInstance] createMovieWithName:self.nameTextView.text category:category];
    [self dismiss];
}

- (IBAction)didPressCancel:(id)sender{
    [self dismiss];
}

- (void)toggleSaveButton:(BOOL) visible{
    self.saveButton.enabled = visible;
    self.saveButton.alpha = visible?1:0.5;
}

- (void)dismiss{
    [self dismissViewControllerAnimated:YES completion:nil];
}

#pragma mark - UIPickerViewDataSource

- (NSInteger)numberOfComponentsInPickerView:(UIPickerView *)pickerView {
    return 1;
}

- (NSInteger)pickerView:(UIPickerView *)pickerView numberOfRowsInComponent:(NSInteger)component {
    return self.categories.count;
}

#pragma mark - UIPickerViewDelegate
-(NSString *) pickerView:(UIPickerView *)pickerView titleForRow:(NSInteger)row forComponent:(NSInteger)component{
    MovieCategory *category = [self.categories objectAtIndex:row];
    return category.name;
}

@end
