
#import <UIKit/UIKit.h>
#import "Movie.h"

@interface RateMovieViewController : UIViewController

@property (nonatomic, strong) Movie *movie;
@property (nonatomic, strong) NSArray *ratings;

@end
