
#import "HomeViewController.h"
#import "MovieTableViewCell.h"
#import "AddMovieViewController.h"
#import "RateMovieViewController.h"
#import "Movie.h"
#import "Rating.h"
#import "MovieCategory.h"
#import "DataManager.h"

@interface HomeViewController ()<UITableViewDataSource, UITableViewDelegate, NSFetchedResultsControllerDelegate>
@property (nonatomic, strong) UITableView *resultsTableView;
@property (nonatomic, strong) NSArray *movies;
@property (nonatomic, strong) NSFetchedResultsController *fetchedResultsController;

@end

@implementation HomeViewController

//Set basic ui
- (void)viewDidLoad {
    [super viewDidLoad];
    self.title = @"Movies";
    UIBarButtonItem *refreshButton = [[UIBarButtonItem alloc]
                                      initWithBarButtonSystemItem:UIBarButtonSystemItemAdd
                                      target:self action:@selector(didPressedAdd)];
    self.navigationItem.rightBarButtonItem = refreshButton;
    
    [self setupTableView];
    
    [self.fetchedResultsController performFetch:nil];
}

//setup screen autolayout and tableview
- (void)setupTableView {
    self.resultsTableView = [UITableView new];
    self.resultsTableView.translatesAutoresizingMaskIntoConstraints = NO;
    [self.view addSubview:self.resultsTableView];
    // Width constraint
    [self.view addConstraint:[NSLayoutConstraint constraintWithItem:self.resultsTableView
                                                          attribute:NSLayoutAttributeWidth
                                                          relatedBy:NSLayoutRelationEqual
                                                             toItem:self.view
                                                          attribute:NSLayoutAttributeWidth
                                                         multiplier:1
                                                           constant:0]];
    
    // Height constraint
    [self.view addConstraint:[NSLayoutConstraint constraintWithItem:self.resultsTableView
                                                          attribute:NSLayoutAttributeHeight
                                                          relatedBy:NSLayoutRelationEqual
                                                             toItem:self.view
                                                          attribute:NSLayoutAttributeHeight
                                                         multiplier:1
                                                           constant:0]];
    
    // Center horizontally
    [self.view addConstraint:[NSLayoutConstraint constraintWithItem:self.resultsTableView
                                                          attribute:NSLayoutAttributeCenterX
                                                          relatedBy:NSLayoutRelationEqual
                                                             toItem:self.view
                                                          attribute:NSLayoutAttributeCenterX
                                                         multiplier:1.0
                                                           constant:0.0]];
    
    // Center vertically
    [self.view addConstraint:[NSLayoutConstraint constraintWithItem:self.resultsTableView
                                                          attribute:NSLayoutAttributeCenterY
                                                          relatedBy:NSLayoutRelationEqual
                                                             toItem:self.view
                                                          attribute:NSLayoutAttributeCenterY
                                                         multiplier:1.0
                                                           constant:0.0]];
    self.resultsTableView.delegate = self;
    self.resultsTableView.dataSource = self;
    [self.resultsTableView registerNib:[UINib nibWithNibName:@"MovieTableViewCell" bundle:[NSBundle mainBundle]]
                forCellReuseIdentifier:[MovieTableViewCell reuseIdentifier]];
}

//user pressed the add button, we show the following screen as a modal
- (void)didPressedAdd{
    AddMovieViewController *addMovie = [[AddMovieViewController alloc] init];
    self.providesPresentationContextTransitionStyle = YES;
    self.definesPresentationContext = YES;
    [addMovie setModalPresentationStyle:UIModalPresentationOverCurrentContext];
    
    [self.navigationController presentViewController:addMovie animated:YES completion:nil];
}

#pragma mark - UITableViewDataSource methods

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    id  sectionInfo = [[_fetchedResultsController sections] objectAtIndex:section];
    return [sectionInfo numberOfObjects];
}

- (UITableViewCell *) tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    MovieTableViewCell *cell = (MovieTableViewCell *)
    [tableView dequeueReusableCellWithIdentifier:[MovieTableViewCell reuseIdentifier] forIndexPath:indexPath];
    
    return [self setupMovieCell:cell atIndexPath:indexPath];
}

- (MovieTableViewCell *)setupMovieCell:(MovieTableViewCell *)cell atIndexPath:(NSIndexPath *)indexPath{
    Movie *movie = [_fetchedResultsController objectAtIndexPath:indexPath];
    NSArray *ratingsArray = [[DataManager sharedInstance] getAllRatingsForMovie:movie];
    CGFloat total = 0;
    
    for (Rating *rating in ratingsArray) {
        total += [rating.quantity floatValue];
    }
    
    cell.nameLabel.text = movie.name;
    cell.categoryLabel.text = movie.movieCategory.name;
    long ratingsCount = ratingsArray.count;
    cell.ratingLabel.text = [NSString stringWithFormat:@"%.2f", ratingsCount>0?(total/ratingsCount):0.0f];

    return cell;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    return [MovieTableViewCell cellHeight];
}

- (BOOL)tableView:(UITableView *)tableView canEditRowAtIndexPath:(NSIndexPath *)indexPath{
    return YES;
}

- (void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (editingStyle == UITableViewCellEditingStyleDelete) {
        Movie *movie = [_fetchedResultsController objectAtIndexPath:indexPath];
        [[DataManager sharedInstance] removeMovie:movie];
    }
}

#pragma mark - UITableViewDelegate methods

- (void) tableView:(UITableView *)tableView didSelectRowAtIndexPath:(nonnull NSIndexPath *)indexPath{
    RateMovieViewController *rateMovieVC = [RateMovieViewController new];
    Movie *currentMovie = [_fetchedResultsController objectAtIndexPath:indexPath];
    rateMovieVC.movie = currentMovie;
    rateMovieVC.ratings =  [[DataManager sharedInstance] getAllRatingsForMovie:currentMovie];
    [self.navigationController pushViewController:rateMovieVC animated:NO];
}

- (BOOL)gestureRecognizer:(UIGestureRecognizer *)gestureRecognizer shouldRecognizeSimultaneouslyWithGestureRecognizer:(UIGestureRecognizer *)otherGestureRecognizer {
    return YES;
}

#pragma mark - NSFetchedResultsControllerDelegate

- (void)controllerWillChangeContent:(NSFetchedResultsController *)controller {
    // The fetch controller is about to start sending change notifications, so prepare the table view for updates.
    [self.resultsTableView beginUpdates];
}

- (void)controller:(NSFetchedResultsController *)controller didChangeObject:(id)anObject atIndexPath:(NSIndexPath *)indexPath forChangeType:(NSFetchedResultsChangeType)type newIndexPath:(NSIndexPath *)newIndexPath {
    UITableView *tableView = self.resultsTableView;
    
    switch(type) {
            
        case NSFetchedResultsChangeInsert:
            [tableView insertRowsAtIndexPaths:[NSArray arrayWithObject:newIndexPath] withRowAnimation:UITableViewRowAnimationFade];
            break;
            
        case NSFetchedResultsChangeDelete:
            [tableView deleteRowsAtIndexPaths:[NSArray arrayWithObject:indexPath] withRowAnimation:UITableViewRowAnimationFade];
            break;
            
        case NSFetchedResultsChangeUpdate:
            [self setupMovieCell:[tableView cellForRowAtIndexPath:indexPath] atIndexPath:indexPath];
            break;
            
        case NSFetchedResultsChangeMove:
            [tableView deleteRowsAtIndexPaths:[NSArray
                                               arrayWithObject:indexPath] withRowAnimation:UITableViewRowAnimationFade];
            [tableView insertRowsAtIndexPaths:[NSArray
                                               arrayWithObject:newIndexPath] withRowAnimation:UITableViewRowAnimationFade];
            break;
    }
}

- (void)controller:(NSFetchedResultsController *)controller didChangeSection:(id )sectionInfo atIndex:(NSUInteger)sectionIndex forChangeType:(NSFetchedResultsChangeType)type {
    switch(type) {
        case NSFetchedResultsChangeInsert:
            [self.resultsTableView insertSections:[NSIndexSet indexSetWithIndex:sectionIndex] withRowAnimation:UITableViewRowAnimationAutomatic];
            break;
        case NSFetchedResultsChangeDelete:
            [self.resultsTableView deleteSections:[NSIndexSet indexSetWithIndex:sectionIndex] withRowAnimation:UITableViewRowAnimationAutomatic];
            break;
        default:
            break;
    }
}


- (NSFetchedResultsController *)fetchedResultsController {
    if (_fetchedResultsController != nil) {
        return _fetchedResultsController;
    }
    
    _fetchedResultsController = [Movie fetchAllGroupedBy:nil withPredicate:nil sortedBy:@"creationDate" ascending:YES];
    _fetchedResultsController.delegate = self;
    
    return _fetchedResultsController;
}


- (void)controllerDidChangeContent:(NSFetchedResultsController *)controller {
    // The fetch controller has sent all current change notifications, so tell the table view to process all updates.
    [self.resultsTableView endUpdates];
}

@end
